(define ADD(lambda (a b)         
		(SUCC a (SUCC b 0)) ))
		 
(define SUB(lambda (a b)         
		(PRED b a) ))
		
(define AND(lambda (M N)         
		 (N (M #T #F) #F)))
		 
(define OR(lambda (M N)         
		 (N #T (M #T #F))))

(define NOT(lambda (M)         
		 (M #F #T)))
		 
(define TRUE(lambda (a b)         
		 a ))
		 
(define FALSE(lambda (a b)         
		 b ))

(define LEQ(lambda (a b)       
		(ISZERO (SUB a b)) ))

(define GEQ(lambda (a b)       
		(ISZERO (SUB b a)) ))
		 
(define (SUCC n z)
	  (define (iter n z)
	    (if (zero? n)
	        z
	        (iter (- n 1) (+ z 1))))
	  (iter n z))
	  
(define (PRED n z)
	  (define (iter n z)
		(if (zero? z)
			z
			(if (zero? n)
				z
				(iter (- n 1) (- z 1)))))
	  (iter n z))
	  
(define (ISZERO n)
	  (if (zero? n)
		#t
		#f
	  ))